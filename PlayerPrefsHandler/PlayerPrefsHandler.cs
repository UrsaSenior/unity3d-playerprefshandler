using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;

/// <summary>
/// Managerial class to assist with creation, serialization and writing of 
/// registry entries to file with the purpose of adding functionality onto Unity's PlayerPrefs system.
/// </summary>
public static class
PlayerPrefsHandler
{
	//
	// class members
	//

#if !UNITY_WEBPLAYER	

	private static Hashtable playerPrefsHashtable = new Hashtable();	// local playerprefs cache structure
		
	private static bool hashTableChanged = false;
	private static string serializedOutput = "";
	private static string serializedInput = "";
		
	private const string PARAMETERS_SEPERATOR = ";";
	private const string KEY_VALUE_SEPERATOR = ":";
		
	private static readonly string fileName = Application.persistentDataPath + "/PlayerPrefs.txt";

#endif	

	//
	// ctor
	//
	
	static
    PlayerPrefsHandler()
	{
#if !UNITY_WEBPLAYER
		//load previous settings
		StreamReader fileReader = null;
			
		if (File.Exists(fileName))
		{
			fileReader = new StreamReader(fileName);
		
			serializedInput = fileReader.ReadLine();
				
			Deserialize();
				
			fileReader.Close();
		}

        Debug.Log("PlayerPrefsHandler writing to " + fileName);
#endif
	}
	
	//
	// class methods
	//
		
	/// <summary>
	/// Determines whether this instance has the specified key.
	/// </summary>
	/// <returns>
	/// <c>true</c> if this instance has key the specified key; otherwise, <c>false</c>.
	/// </returns>
	/// <param name='key'>
	/// If set to <c>true</c> key.
	/// </param>
	public static bool
    HasKey(string key)
	{	
#if UNITY_WEBPLAYER
        return IntToBool(PlayerPrefs.GetInt(key));
#else
		return playerPrefsHashtable.ContainsKey(key);
#endif
	}
		
	/// <summary>
	/// Sets a new or existing key of String type
	/// </summary>
	/// <param name='key'>
	/// Key name
	/// </param>
	/// <param name='value'>
	/// Key value to be assigned
	/// </param>
	public static void
    SetString(string key, string value)
	{
#if UNITY_WEBPLAYER
        PlayerPrefs.SetString(key, value);
#else
		if(!playerPrefsHashtable.ContainsKey(key))
		{
			playerPrefsHashtable.Add(key, value);
		}
		else
		{
			playerPrefsHashtable[key] = value;
		}
			
		hashTableChanged = true;
#endif
	}
		
	/// <summary>
	/// Sets a new or existing key of Integer type
	/// </summary>
	/// <param name='key'>
	/// Key name
	/// </param>
	/// <param name='value'>
	/// Key value to be assigned
	/// </param>
	public static void
    SetInt(string key, int value)
	{
#if UNITY_WEBPLAYER
        PlayerPrefs.SetInt(key, value);
#else
		if(!playerPrefsHashtable.ContainsKey(key))
		{
			playerPrefsHashtable.Add(key, value);
		}
		else
		{
			playerPrefsHashtable[key] = value;
		}
			
		hashTableChanged = true;
#endif
	}
		
	/// <summary>
	/// Sets a new or existing key of Float type
	/// </summary>
	/// <param name='key'>
	/// Key name
	/// </param>
	/// <param name='value'>
	/// Key value to be assigned
	/// </param>
	public static void
    SetFloat(string key, float value)
	{
#if UNITY_WEBPLAYER
        PlayerPrefs.SetFloat(key, value);
#else
		if(!playerPrefsHashtable.ContainsKey(key))
		{
			playerPrefsHashtable.Add(key, value);
		}
		else
		{
			playerPrefsHashtable[key] = value;
		}
			
		hashTableChanged = true;
#endif
	}
		
	/// <summary>
	/// Sets a new or existing key of Boolean type
	/// </summary>
	/// <param name='key'>
	/// Key name
	/// </param>
	/// <param name='value'>
	/// Key value to be assigned
	/// </param>
	public static void
    SetBool(string key, bool value)
	{
#if UNITY_WEBPLAYER
        PlayerPrefs.SetInt(key, BoolToInt(value));
#else
		if(!playerPrefsHashtable.ContainsKey(key))
		{
			playerPrefsHashtable.Add(key, value);
		}
		else
		{
			playerPrefsHashtable[key] = value;
		}
			
		hashTableChanged = true;
#endif
    }
		
	/// <summary>
	/// Gets the String value of a specified key
	/// </summary>
	/// <returns>
	/// Key Value
	/// </returns>
	/// <param name='key'>
	/// Key name
	/// </param>
	public static string
    GetString(string key)
	{		
#if UNITY_WEBPLAYER
        return PlayerPrefs.GetString(key, null);
#else
		if(playerPrefsHashtable.ContainsKey(key))
		{
			return playerPrefsHashtable[key].ToString();
		}
			
		return null;
#endif
	}
		
	/// <summary>
	/// Gets the String value of a specified key
	/// </summary>
	/// <returns>
	/// Key Value
	/// </returns>
	/// <param name='key'>
	/// Key name
	/// </param>
	/// <param name='defaultValue'>
	/// Default value to be assigned if no value is found
	/// </param>
	public static string
    GetString(string key, string defaultValue)
	{
#if UNITY_WEBPLAYER
        return PlayerPrefs.GetString(key, defaultValue);
#else
		if(playerPrefsHashtable.ContainsKey(key))
		{
			return playerPrefsHashtable[key].ToString();
		}
		else
		{
			playerPrefsHashtable.Add(key, defaultValue);
			hashTableChanged = true;
			return defaultValue;
		}
#endif
	}
	
	/// <summary>
	/// Gets the Integer value of a specified key
	/// </summary>
	/// <returns>
	/// Key Value
	/// </returns>
	/// <param name='key'>
	/// Key name
	/// </param>
	public static int
    GetInt(string key)
	{		
#if UNITY_WEBPLAYER
        return PlayerPrefs.GetInt(key, 0);
#else
		if(playerPrefsHashtable.ContainsKey(key))
		{
			return (int) playerPrefsHashtable[key];
		}
			
		return 0;
#endif
	}
		
	/// <summary>
	/// Gets the Integer value of a specified key
	/// </summary>
	/// <returns>
	/// Key Value
	/// </returns>
	/// <param name='key'>
	/// Key name
	/// </param>
	/// <param name='defaultValue'>
	/// Default value to be used if no key is found
	/// </param>
	public static int
    GetInt(string key, int defaultValue)
	{
#if UNITY_WEBPLAYER
        return PlayerPrefs.GetInt(key, defaultValue);
#else
		if(playerPrefsHashtable.ContainsKey(key))
		{
			return (int) playerPrefsHashtable[key];
		}
		else
		{
			playerPrefsHashtable.Add(key, defaultValue);
			hashTableChanged = true;
			return defaultValue;
		}
#endif
	}
		
	/// <summary>
	/// Gets the Float value of a specified key
	/// </summary>
	/// <returns>
	/// Key Value
	/// </returns>
	/// <param name='key'>
	/// Key name
	/// </param>
	public static float
    GetFloat(string key)
	{			
#if UNITY_WEBPLAYER
        return PlayerPrefs.GetFloat(key, 0.0f);
#else
		if(playerPrefsHashtable.ContainsKey(key))
		{
			return (float) playerPrefsHashtable[key];
		}
			
		return 0.0f;
#endif
	}
		
	/// <summary>
	/// Gets the Float value of a specified key
	/// </summary>
	/// <returns>
	/// Key Value
	/// </returns>
	/// <param name='key'>
	/// Key name
	/// </param>
	/// <param name='defaultValue'>
	/// Default value to be assigned if none is found
	/// </param>
	public static float
    GetFloat(string key, float defaultValue)
	{
#if UNITY_WEBPLAYER
        return PlayerPrefs.GetFloat(key, defaultValue);
#else
		if(playerPrefsHashtable.ContainsKey(key))
		{
			return (float) playerPrefsHashtable[key];
		}
		else
		{
			playerPrefsHashtable.Add(key, defaultValue);
			hashTableChanged = true;
			return defaultValue;
		}
#endif
	}
		
	/// <summary>
	/// Gets the Boolean value of a specified key
	/// </summary>
	/// <returns>
	/// Key Value
	/// </returns>
	/// <param name='key'>
	/// Key name
	/// </param>
	public static bool
    GetBool(string key)
	{		
#if UNITY_WEBPLAYER
        return IntToBool(PlayerPrefs.GetInt(key));
#else
		if(playerPrefsHashtable.ContainsKey(key))
		{
			return (bool) playerPrefsHashtable[key];
		}
			
		return false;
#endif
	}
		
	/// <summary>
	/// Gets the Boolean value of a specified key
	/// </summary>
	/// <returns>
	/// Key Value
	/// </returns>
	/// <param name='key'>
	/// Key name
	/// </param>
	/// <param name='defaultValue'>
	/// Default value to be assigned if no key is found
	/// </param>
	public static bool
    GetBool(string key, bool defaultValue)
	{
#if UNITY_WEBPLAYER
        const int FAKE_INT_DEFAULT = Int32.MinValue;
        int _integer = PlayerPrefs.GetInt(key, FAKE_INT_DEFAULT);

        if (_integer == FAKE_INT_DEFAULT)
            return defaultValue;

        return IntToBool(_integer);
#else
		if(playerPrefsHashtable.ContainsKey(key))
		{
			return (bool) playerPrefsHashtable[key];
		}
		else
		{
			playerPrefsHashtable.Add(key, defaultValue);
			hashTableChanged = true;
			return defaultValue;
		}
#endif
	}
		
	/// <summary>
	/// Deletes a saved key.
	/// </summary>
	/// <param name='key'>
	/// Key name
	/// </param>
	public static void
    DeleteKey(string key)
	{
#if UNITY_WEBPLAYER
        PlayerPrefs.DeleteKey(key);
#else
		playerPrefsHashtable.Remove(key);
#endif
	}
	
	/// <summary>
	/// Deletes all key entries
	/// </summary>
	public static void
    DeleteAll()
	{
#if UNITY_WEBPLAYER
        PlayerPrefs.DeleteAll();
#else
		playerPrefsHashtable.Clear();
#endif
	}
		
	/// <summary>
	/// Flush the local PlayerPrefs entries to file on disk
	/// </summary>
	public static void
    Flush()
	{
#if UNITY_WEBPLAYER
        PlayerPrefs.Save();
#else
		if(hashTableChanged)
		{
			Serialize();
				
			StreamWriter fileWriter = null;
            fileWriter = File.CreateText(fileName);
			
			if (fileWriter == null)
			{ 
				Debug.LogWarning("PlayerPrefs::Flush() opening file for writing failed: " + fileName);
			}
				
			fileWriter.WriteLine(serializedOutput);
				
			fileWriter.Close();

			serializedOutput = "";
		}
#endif
	}
		
	/// <summary>
	/// Serialize this playerPrefs entries to be written
	/// </summary>
	private static void
    Serialize()
	{
#if !UNITY_WEBPLAYER
		IDictionaryEnumerator myEnumerator = playerPrefsHashtable.GetEnumerator();
			
		while ( myEnumerator.MoveNext() )
		{
			if(serializedOutput != "")
			{
				serializedOutput += " " + PARAMETERS_SEPERATOR + " ";
			}
			serializedOutput += SerializeEscapeNonSeperators(myEnumerator.Key.ToString()) + " " + KEY_VALUE_SEPERATOR + " " + SerializeEscapeNonSeperators(myEnumerator.Value.ToString()) + " " + KEY_VALUE_SEPERATOR + " " + myEnumerator.Value.GetType();
		}
#endif
	}
		
	/// <summary>
	/// Deserialize this playerPrefs entries to be written
	/// </summary>
	private static void
    Deserialize()
	{
#if !UNITY_WEBPLAYER
		string[] parameters = serializedInput.Split(new string[] {" " + PARAMETERS_SEPERATOR + " "}, StringSplitOptions.None);
			
		foreach(string parameter in parameters)
		{
			string[] parameterContent = parameter.Split(new string[]{" " + KEY_VALUE_SEPERATOR + " "}, StringSplitOptions.None);
				
			playerPrefsHashtable.Add(DeserializeEscapeNonSeperators(parameterContent[0]), GetTypeValue(parameterContent[2], DeserializeEscapeNonSeperators(parameterContent[1])));
				
			if(parameterContent.Length > 3)
			{
				Debug.LogWarning("PlayerPrefs::Deserialize() parameterContent has " + parameterContent.Length + " elements");
			}
		}
#endif
	}

#if !UNITY_WEBPLAYER	
	/// <summary>
	/// Prepares the seperator strings in all instances for serialization
	/// </summary>
	/// <returns>
	/// The input that will cause the escape sequence
	/// </returns>
	/// <param name='escapeInput'>
	/// String seperator sequence
	/// </param>
	private static string
    SerializeEscapeNonSeperators(string escapeInput)
	{
		escapeInput = escapeInput.Replace(KEY_VALUE_SEPERATOR,"\\" + KEY_VALUE_SEPERATOR);
		escapeInput = escapeInput.Replace(PARAMETERS_SEPERATOR,"\\" + PARAMETERS_SEPERATOR);
		return escapeInput;
	}

		
	/// <summary>
    /// Prepares the seperator strings in all instances for deserialization
	/// </summary>
	/// <returns>
	/// The input that will cause the escape sequence
	/// </returns>
	/// <param name='inputToDeEscape'>
	/// Input to undo escape.
	/// </param>
	private static string
    DeserializeEscapeNonSeperators(string escapeInput)
	{
        escapeInput = escapeInput.Replace("\\" + KEY_VALUE_SEPERATOR, KEY_VALUE_SEPERATOR);
        escapeInput = escapeInput.Replace("\\" + PARAMETERS_SEPERATOR, PARAMETERS_SEPERATOR);
		return escapeInput;
	}
#endif

    /// <summary>
	/// Gets the type value for a specific type name.
	/// </summary>
	/// <returns>
	/// The type value.
	/// </returns>
	/// <param name='typeName'>
	/// Type name.
	/// </param>
	/// <param name='value'>
	/// Key Value.
	/// </param>
	public static object
    GetTypeValue(string typeName, string value)
	{
        if (typeName == "string") return (object)value.ToString();

        if (typeName == "int") return (object)System.Convert.ToInt32(value);

        if (typeName == "bool") return (object)System.Convert.ToBoolean(value);

		if (typeName == "float") return (object)System.Convert.ToSingle(value);

	    Debug.LogError("PlayerPrefsHandler encountered unsupported type: " + typeName);

		return null;
	}

#if UNITY_WEBPLAYER
    private static bool
    IntToBool(int value) { return value != 0; }

    private static int
    BoolToInt(bool value) { return value ? 1 : 0; }
#endif
}	